/**
 * This is a class for testing some methods in the Department class
 * it tests both negative and positive tests for the removePerson- method,
 * and also a negative test for the addPerson method
 */
package no.ntnu.vildegy;

import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertTrue;

class DepartmentTest {

    Department testDepartment = new Department("testDepartment");

    @Test
    void testRemoveSuccessPatient() {

        Patient ida = new Patient("Ida", "Heglund", "11256784567");
        testDepartment.addPatient(ida); //adder ida i listen, derfor blir testen success

        try {
            testDepartment.removePerson(ida);
            assertTrue(true);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }


    @Test
    void testRemoveSuccessEmployee() {

        Employee heidi = new Employee("Heidi", "Gundersen", "65467865432");
        testDepartment.addEmployee(heidi); //adder heidi i lista, derfor blir testen unsuccessful

        try {
            testDepartment.removePerson(heidi);
            assertTrue(true);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }


    @Test
    void testRemoveUnsuccessfulEmployee() {

        //adder ikke vilde i noen liste, derfor skal den kaste unntak
        Employee vilde = new Employee("Vilde", "Gylterud", "67865443345");

        try {
            testDepartment.removePerson(vilde);
            assertTrue(false);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }


    @Test
    void testRemoveNullPatient() {

        Patient nullPatient = null;
        //et nullobjekt som prøves å remove skal kaste et unntak

        try {
            testDepartment.removePerson(nullPatient);
            assertTrue(false);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }


    @Test
    void testAddPersonWithExistingSSN() {

        //Det skal ikke være mulig å adde noen som har ulikt navn, men samme personnummer.

        Patient nora = new Patient("Nora", "Gylterud","5" ); //SSN = 5

        try {
            testDepartment.addPatient(nora);
            assertTrue(true);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            assertTrue(false);
        }

        Patient mari = new Patient("Mari", "Gylterud", "5"); //SSN = 5

        try {
            testDepartment.addPatient(mari);
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }
}
