/**
 * A department has a name, different employees and different patient
 */
package no.ntnu.vildegy;

import java.util.ArrayList;
import java.util.Objects;

public class Department {

    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;


    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.employees = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    public void setDepartmentName(String departmentName) {

        this.departmentName = departmentName;
    }

    public String getDepartmentName() {

        return departmentName;
    }

    public ArrayList<Employee> getEmployees() {

        return employees;
    }

    public ArrayList<Patient> getPatients() {

        return patients;
    }

    /**
     * adds a new employee to the list of employees
     * if a employee with the same SSN exist an exception will be thrown
     *
     * @param employee
     */
    public void addEmployee(Employee employee) {
        for (Employee e : employees) {
            if (employee.getSocialSecurityNumber().equals(e.getSocialSecurityNumber())) { //the SSN is unique, therefore i only check if the person exist from the getSocialSecurityNumber
                throw new IllegalArgumentException("A employee with the same social security number already exist in our system");
            } else {
                employees.add(employee);
            }
        }
    }

    /**
     * adds a new patient to the list of patients
     * if a patient with the same SSN exist an exception will be thrown
     *
     * @param patient
     */
    public void addPatient(Patient patient) {
        for (Patient p : patients) {
            if (patient.getSocialSecurityNumber().equals(p.getSocialSecurityNumber())) { //the SSN is unique, therefore i only check if the person exist from the getSocialSecurityNumber
                throw new IllegalArgumentException("A patient with the same social security number already excist in our system");
            } else {
                patients.add(patient);
            }
        }
    }

    /**
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }


    /**
     * removes a person (employee or a patient) from the associated list
     *
     * @param p the person the user want to remove
     * @throws RemoveException if input is null, or the person could not be found in the list an RemoveException will be thrown
     */
    public void removePerson(Person p) throws RemoveException {
        try {
            if (p == null) {
                throw new RemoveException("Input can not be null");
            } else if (employees.contains(p) && p instanceof Employee) { //checks if the persons are in the employees list and is a employee
                employees.remove(p);
            } else if (patients.contains(p) && p instanceof Patient) { //checks if the persons are in the patient list and is a patient
                patients.remove(p);
            } else {
                throw new RemoveException("The person you want to remove could not be found!");
            }
        } catch (RemoveException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @param o, the reference object with which to compare
     * @return true if this object is the same as the obj argument; false otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return departmentName.equals(that.departmentName);
    }

    @Override
    public String toString() {
        return "\n" + "DepartmentName: " + departmentName + "\n" +
                "Employees: " + employees + "\n" +
                "Patients: " + patients + "\n";
    }
}



