/**
 * RemoveException
 */
package no.ntnu.vildegy;

import java.io.Serial;
import java.io.Serializable;

public class RemoveException extends Exception implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public RemoveException(final String message) {
        super(message);
    }
}
