/**
 * this abstract class extends the the class Employee
 */
package no.ntnu.vildegy;

public abstract class Doctor extends Employee {

    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /** an abstract method for setting diagnosis on patient
     *
     * @param patient - correct diagnosis to the right patient
     * @param diagnosis a String with the patient´s diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);

}

