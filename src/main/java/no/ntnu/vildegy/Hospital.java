/**
 * A hospital with a name and different departments
 */
package no.ntnu.vildegy;

import java.util.ArrayList;

public class Hospital {

    private String hospitalName;
    private ArrayList<Department> departments;

    /**
     * create the department list in the constructor
     * @param hospitalName
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        this.departments = new ArrayList<>();
    }

    public String getHospitalName() {

        return hospitalName;
    }

    public ArrayList<Department> getDepartments() {

        return departments;
    }

    /**
     * adds a new department in the departments list, if its doesnt exist already
     * @param department
     */
    public void addDepartments(Department department) {
        if(!departments.contains(department)){ //checks if the department exist in the departments-list
            departments.add(department); //if not, the new department will be added to the list
        } else if(department == null) {
            throw new IllegalArgumentException("SKRIV DENNE SENERE");
        }
        throw new IllegalArgumentException("This department already excist!"); // if it exist, an exception is thrown to the user
        }


    @Override
    public String toString() {
        return "\n" + "Hospital: " + "\n" +
                "HospitalName: '" + hospitalName + "\n" +
                "Departments: " + departments + "\t";
    }
}
