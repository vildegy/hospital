/**
 * this class extends the abstract class Doctor
 */
package no.ntnu.vildegy;

public class Surgeon extends Doctor {

    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     *
     * @param patient - correct diagnosis to the right patient
     * @param diagnosis a String with the patient´s diagnosis
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
