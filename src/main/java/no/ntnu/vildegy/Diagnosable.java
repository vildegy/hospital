/**
 *  Interface class for setting diagnosis on patients
 */
package no.ntnu.vildegy;

public interface Diagnosable {

    /**
     * abstract method for setting the diagnosis of the patient
     */
    public abstract void setDiagnosis(String diagnosis);
}
