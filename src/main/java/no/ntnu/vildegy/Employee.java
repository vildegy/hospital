/**
 * This class extends the abstract class Person
 */
package no.ntnu.vildegy;

public class Employee extends Person{


    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @return the first name, last name and the SSN to the employee
     */
    @Override
    public String toString() {
        return super.toString();
    }
}


