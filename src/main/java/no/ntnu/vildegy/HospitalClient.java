/**
 * Enkelt klientprogram for Hospital-prosjektet, slik oppgaven beskriver
 */
package no.ntnu.vildegy;

public class HospitalClient {

    public static void main(String[] args) throws RemoveException {

        Hospital hospital = new Hospital("Tønsberg sykehus");

        HospitalTestData.fillRegisterWithTestData(hospital);

        Department department = hospital.getDepartments().get(1); //gir meg department: "children polyclinic"


        /*
         * Fjerne en ansatt som FINNES i department-listen med remove-metoden
         */
        Employee salti = department.getEmployees().get(0); //henter første person i listen, altså salti

        try {
            department.removePerson(salti);
        } catch (RemoveException removeException) {
            removeException.printStackTrace();
        }


        /*
         *  Fjerne en pasient som IKKE FINNES i department-listen
         */
        Patient fiktivPerson = new Patient("Fiktiv", "Person", "40");

        try {
            department.removePerson(fiktivPerson);
        } catch (RemoveException removeException) {
            removeException.printStackTrace();
        }

        System.out.println(department.toString()); //skriver ut for å se at salti ikke lenger finnes i listen
    }
}

